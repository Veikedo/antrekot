using System.IO.IsolatedStorage;
using Antrekot.Services.Settings;

namespace Antrekot.MainApp.Services
{
  public class SettingsService : ISettingsService
  {
    private readonly IsolatedStorageSettings _isolatedStorage;

    private const string ZoomLevelKey = "ZoomLevel";
    private const string CultureKey = "Culture";
    private const string NoticeRadiusKey = "NoticeRadius";

    private const double NoticeRadiusOrigin = 1000;
    private const double ZoomLevelOrigin = 10;
    private const string CultureOrigin = "en";

    public SettingsService()
    {
      _isolatedStorage = IsolatedStorageSettings.ApplicationSettings;

      if (!_isolatedStorage.Contains(ZoomLevelKey))
      {
        ZoomLevel = ZoomLevelOrigin;
      }

      if (!_isolatedStorage.Contains(CultureKey))
      {
        Culture = CultureOrigin;
      }

      if (!_isolatedStorage.Contains(NoticeRadiusKey))
      {
        NoticeRadius = NoticeRadiusOrigin;
      }
    }

    public string Culture
    {
      get { return _isolatedStorage[CultureKey] as string; }
      set { _isolatedStorage[CultureKey] = value; }
    }

    public double ZoomLevel
    {
      get { return (double) _isolatedStorage[ZoomLevelKey]; }
      set { _isolatedStorage[ZoomLevelKey] = value; }
    }

    public double NoticeRadius
    {
      get { return (double) _isolatedStorage[NoticeRadiusKey]; }
      set { _isolatedStorage[NoticeRadiusKey] = value; }
    }
  }
}