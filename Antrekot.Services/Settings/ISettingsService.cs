﻿namespace Antrekot.Services.Settings
{
  public interface ISettingsService
  {
    /// <summary>
    /// Current app culture
    /// </summary>
    string Culture { get; set; }

    /// <summary>
    /// Map zoom level
    /// </summary>
    double ZoomLevel { get; set; }

    /// <summary>
    /// Task notification radius
    /// </summary>
    double NoticeRadius { get; set; }
  }
}