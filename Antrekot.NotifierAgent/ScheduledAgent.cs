﻿using System;
using System.Device.Location;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using Antrekot.MainApp.Services;
using Antrekot.NotifierAgent.Resources;
using Antrekot.Repository.Models;
using Antrekot.Repository.Repository;
using Antrekot.Services.Settings;
using Cimbalino.Phone.Toolkit.Services;
using Microsoft.Phone.Scheduler;

namespace Antrekot.NotifierAgent
{
  public class ScheduledAgent : ScheduledTaskAgent
  {
    private readonly ILocationService _locationService;
    private readonly IRepository _repository;
    private readonly ISettingsService _settingsService;

    static ScheduledAgent()
    {
      Deployment.Current.Dispatcher.BeginInvoke(() => Application.Current.UnhandledException += UnhandledException);
    }

    public ScheduledAgent()
    {
      _settingsService = new SettingsService();
      _repository = new SqlRepository("Data source=isostore:/mapTasks.sdf");
      _locationService = new LocationService();
    }

    private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
    {
      if (Debugger.IsAttached)
      {
        Debugger.Break();
      }
    }

    protected override async void OnInvoke(ScheduledTask task)
    {
      var position = await _locationService.GetPositionAsync(LocationServiceAccuracy.Default);
      GeoCoordinate currentPosition = MapPositionToGeoCoordinate(position);

      SetLocale();

      var nearbyTasks = _repository.MapTasks.AsEnumerable()
                                   .Where(x => x.NoticeEnabled &&
                                               CalcDistanseToTask(x, currentPosition) <= _settingsService.NoticeRadius)
                                   .ToList();

      if (nearbyTasks.Any())
      {
        var toastService = new ShellToastService();
        string toast;

        if (nearbyTasks.Count == 1)
        {
          MapTask mapTask = nearbyTasks.First();

          double distInKms = CalcDistanseToTask(mapTask, currentPosition)/1000;
          toast = string.Format(Resource.ToastSingleTask, mapTask.Title, distInKms);
        }
        else
        {
          toast = Resource.ToastManyTasks;
        }

        toastService.Show(Resource.ToastTitle, toast);
      }

      NotifyComplete();
    }

    private double CalcDistanseToTask(MapTask x, GeoCoordinate currentPosition)
    {
      return new GeoCoordinate(x.Latitude, x.Longtitude).GetDistanceTo(currentPosition);
    }

    private void SetLocale()
    {
      string culture = _settingsService.Culture;

      Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
    }

    private GeoCoordinate MapPositionToGeoCoordinate(LocationServicePosition position)
    {
      return new GeoCoordinate(position.Latitude, position.Longitude);
    }
  }
}