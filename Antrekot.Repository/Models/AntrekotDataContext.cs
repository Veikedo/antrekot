﻿using System.Data.Linq;

namespace Antrekot.Repository.Models
{
  public class AntrekotDataContext : DataContext
  {
    public AntrekotDataContext(string connectionString) : base(connectionString)
    {
      if (!DatabaseExists())
      {
        CreateDatabase();
        SubmitChanges();
      }
      
      MapTasks = GetTable<MapTask>();
    }

    public Table<MapTask> MapTasks { get; set; }
  }
}