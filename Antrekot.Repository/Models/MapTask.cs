﻿using System;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Runtime.CompilerServices;

namespace Antrekot.Repository.Models
{
  [Table]
  public class MapTask : INotifyPropertyChanging, INotifyPropertyChanged
  {
    private const double Tolerance = 1e-4;
    private DateTime _creationTime;
    private string _description;
    private double _latitude;
    private double _longtitude;
    private int _taskId;
    private string _title;
    private bool _noticeEnabled;

    [Column(DbType = "INT NOT NULL Identity", IsDbGenerated = true, IsPrimaryKey = true)]
    public int TaskId
    {
      get { return _taskId; }
      set
      {
        if (_taskId != value)
        {
          RaisePropertyChanging();
          _taskId = value;
          RaisePropertyChanging();
        }
      }
    }

    [Column]
    public string Title
    {
      get { return _title; }
      set
      {
        if (_title != value)
        {
          RaisePropertyChanging();
          _title = value;
          RaisePropertyChanged();
        }
      }
    }

    [Column]
    public string Description
    {
      get { return _description; }
      set
      {
        if (_description != value)
        {
          RaisePropertyChanging();
          _description = value;
          RaisePropertyChanged();
        }
      }
    }

    [Column]
    public double Latitude
    {
      get { return _latitude; }
      set
      {
        if (Math.Abs(_latitude - value) > Tolerance)
        {
          RaisePropertyChanging();
          _latitude = value;
          RaisePropertyChanged();
        }
      }
    }

    [Column]
    public double Longtitude
    {
      get { return _longtitude; }
      set
      {
        if (Math.Abs(_longtitude - value) > Tolerance)
        {
          RaisePropertyChanging();
          _longtitude = value;
          RaisePropertyChanged();
        }
      }
    }

    [Column]
    public bool NoticeEnabled
    {
      get { return _noticeEnabled; }
      set
      {
        if (_noticeEnabled != value)
        {
          RaisePropertyChanging();
          _noticeEnabled = value;
          RaisePropertyChanged();
        }
      }
    }

    [Column]
    public DateTime CreationTime
    {
      get { return _creationTime; }
      set
      {
        if (_creationTime != value)
        {
          RaisePropertyChanging();
          _creationTime = value;
          RaisePropertyChanged();
        }
      }
    }

    #region Notify members

    public event PropertyChangedEventHandler PropertyChanged;
    public event PropertyChangingEventHandler PropertyChanging;

    protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
    {
      var handler = PropertyChanged;
      if (handler != null)
      {
        handler(this, new PropertyChangedEventArgs(propertyName));
      }
    }

    protected void RaisePropertyChanging([CallerMemberName] string propertyName = "")
    {
      var handler = PropertyChanging;
      if (handler != null)
      {
        handler(this, new PropertyChangingEventArgs(propertyName));
      }
    }

    #endregion
  }
}