﻿using System.Linq;
using Antrekot.Repository.Models;

namespace Antrekot.Repository.Repository
{
  public class SqlRepository : IRepository
  {
    private readonly AntrekotDataContext _db;

    public SqlRepository(string connectionString)
    {
      _db = new AntrekotDataContext(connectionString);
    }

    #region map tasks

    public IQueryable<MapTask> MapTasks
    {
      get { return _db.MapTasks; }
    }

    public bool CreateMapTask(MapTask instance)
    {
      if (instance.TaskId == 0)
      {
        _db.MapTasks.InsertOnSubmit(instance);
        _db.MapTasks.Context.SubmitChanges();
        return true;
      }

      return false;
    }

    public bool UpdateMapTask(MapTask instance)
    {
      MapTask cache = _db.MapTasks.FirstOrDefault(p => p.TaskId == instance.TaskId);

      if (cache != null)
      {
        cache.Description = instance.Description;
        cache.Latitude = instance.Latitude;
        cache.Longtitude = instance.Longtitude;
        cache.Title = instance.Title;

        _db.MapTasks.Context.SubmitChanges();
        return true;
      }

      return false;
    }

    public bool RemoveMapTask(int mapTaskId)
    {
      MapTask instance = _db.MapTasks.FirstOrDefault(p => p.TaskId == mapTaskId);

      if (instance != null)
      {
        _db.MapTasks.DeleteOnSubmit(instance);
        _db.MapTasks.Context.SubmitChanges();
        return true;
      }

      return false;
    }

    #endregion
  }
}