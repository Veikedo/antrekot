﻿using System.Linq;
using Antrekot.Repository.Models;

namespace Antrekot.Repository.Repository
{
  public interface IRepository
  {
    #region MapTask

    /// <summary>
    /// Returns available tasks
    /// </summary>
    IQueryable<MapTask> MapTasks { get; }

    /// <summary>
    /// Creates new task
    /// </summary>
    bool CreateMapTask(MapTask instance);

    /// <summary>
    /// Updates existing task
    /// </summary>
    bool UpdateMapTask(MapTask instance);

    /// <summary>
    /// Removes task
    /// </summary>
    bool RemoveMapTask(int mapTaskId);

    #endregion
  }
}