﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Antrekot.MainApp.Converters
{
  /// <summary>
  ///   Converts pushpin proximity to respective brush
  /// </summary>
  public class PushpinProximityToBrushConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var isNearby = (bool) value;
      return isNearby ? new SolidColorBrush(Colors.Orange) : new SolidColorBrush(Colors.Gray);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}