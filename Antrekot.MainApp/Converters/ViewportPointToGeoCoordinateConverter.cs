﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Microsoft.Phone.Maps.Controls;

namespace Antrekot.MainApp.Converters
{
  /// <summary>
  /// Converts position of map's click to respective coordinate
  /// </summary>
  public class ViewportPointToGeoCoordinateConverter : IEventArgsConverter
  {
    public object Convert(object value, object parameter)
    {
      var args = (GestureEventArgs) value;
      var map = (Map) parameter;

      Point point = args.GetPosition(map);
      return map.ConvertViewportPointToGeoCoordinate(point);
    }
  }
}