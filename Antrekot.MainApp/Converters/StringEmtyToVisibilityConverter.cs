﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Antrekot.MainApp.Helpers;

namespace Antrekot.MainApp.Converters
{
  /// <summary>
  /// If string is null or empty returns Visibility.Collapsed, otherwise Visible
  /// </summary>
  public class StringEmtyToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var str = (string) value;
      return str.IsNullOrEmpty() ? Visibility.Collapsed : Visibility.Visible;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
