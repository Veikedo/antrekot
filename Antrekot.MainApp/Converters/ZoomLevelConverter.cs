﻿using System;
using System.Globalization;
using System.Windows.Data;
using Antrekot.MainApp.Helpers;

namespace Antrekot.MainApp.Converters
{
  /// <summary>
  /// Clamp value to zoomLevel bounds
  /// </summary>
  public class ZoomLevelConverter : IValueConverter
  {
    private const int ZoomLevelMin = 1;
    private const int ZoomLevelMax = 20;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var level = (double) value;
      return MathHelper.Clamp(level, ZoomLevelMin, ZoomLevelMax);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}