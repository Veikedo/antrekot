﻿using ImageTools.IO.Gif;
using Microsoft.Phone.Controls;

namespace Antrekot.MainApp.Views
{
  public partial class MainPage : PhoneApplicationPage
  {
    public MainPage()
    {
      InitializeComponent();
      ImageTools.IO.Decoders.AddDecoder<GifDecoder>();
    }
  }
}