using System;
using System.Globalization;
using System.Threading;
using Cimbalino.Phone.Toolkit.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.ViewModel
{
  public abstract class BaseViewModel : ViewModelBase
  {
    protected readonly INavigationService NavigationService;

    protected BaseViewModel(INavigationService navigationService, IMessenger messenger) : base(messenger)
    {
      NavigationService = navigationService;
    }

    private void NavigateTo(string path)
    {
      var uri = new Uri(path, UriKind.Relative);
      NavigationService.NavigateTo(uri);
    }

    protected void NavigateToMapPage()
    {
      NavigateTo("/Views/MapPage.xaml");
    }

    protected void NavigateToDetailsPage()
    {
      NavigateTo("/Views/TaskDetailsPage.xaml");
    }

    protected void NavigateToTaskListPage()
    {
      NavigateTo("/Views/TaskListPage.xaml");
    }

    protected void NavigateToSettingsPage()
    {
      NavigateTo("/Views/SettingsPage.xaml");
    }

    protected void SetCurrentCulture(string culture)
    {
      Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
    }
  }
}