﻿using Antrekot.MainApp.Messages;
using Antrekot.MainApp.Services;
using Antrekot.MainApp.ViewModel.PageViewModels;
using Antrekot.Repository.Repository;
using Antrekot.Services.Settings;
using Cimbalino.Phone.Toolkit.Services;
using CommonServiceLocator.NinjectAdapter;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using Ninject;

namespace Antrekot.MainApp.ViewModel
{
  public class ViewModelLocator
  {
    static ViewModelLocator()
    {
      var kernel = new StandardKernel();

      kernel.Bind<IRepository>().To<SqlRepository>().InSingletonScope()
            .WithConstructorArgument("connectionString", "Data source=isostore:/mapTasks.sdf");

      kernel.Bind<IMessageBoxService>().To<MessageBoxService>().InSingletonScope();
      kernel.Bind<INavigationService>().To<NavigationService>().InSingletonScope();
      kernel.Bind<IMessenger>().To<Messenger>().InSingletonScope();
      kernel.Bind<ILocationService>().To<LocationService>().WithPropertyValue("MovementThreshold", 10);
      kernel.Bind<ISettingsService>().To<SettingsService>().InSingletonScope();

      kernel.Bind<MapPageViewModel>().To<MapPageViewModel>().InSingletonScope();
      kernel.Bind<TaskDetailsPageViewModel>().To<TaskDetailsPageViewModel>().InSingletonScope();
      kernel.Bind<TaskListPageViewModel>().To<TaskListPageViewModel>().InSingletonScope();
      kernel.Bind<SettingsPageViewModel>().To<SettingsPageViewModel>().InSingletonScope();
      
      ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(kernel));

      // poor hack to init messenger
      ServiceLocator.Current.GetInstance<TaskDetailsPageViewModel>();
    }

    public MapPageViewModel Map
    {
      get { return ServiceLocator.Current.GetInstance<MapPageViewModel>(); }
    }

    public TaskDetailsPageViewModel TaskDetails
    {
      get { return ServiceLocator.Current.GetInstance<TaskDetailsPageViewModel>(); }
    }

    public TaskListPageViewModel TaskList
    {
      get { return ServiceLocator.Current.GetInstance<TaskListPageViewModel>(); }
    }

    public SettingsPageViewModel Settings
    {
      get { return ServiceLocator.Current.GetInstance<SettingsPageViewModel>(); }
    }

    public static void Cleanup()
    {
      var message = new ApplicationClosingMessage();
      ServiceLocator.Current.GetInstance<IMessenger>().Send(message);
    }
  }
}