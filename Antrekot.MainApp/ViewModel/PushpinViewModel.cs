﻿using System.ComponentModel;
using System.Device.Location;
using System.Windows.Input;
using Antrekot.MainApp.Messages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Phone.Maps.Controls;

namespace Antrekot.MainApp.ViewModel
{
  /// <summary>
  /// Represents pushpin on map control
  /// </summary>
  public class PushpinViewModel : ViewModelBase
  {
    private bool _isNearby;
    private GeoCoordinate _location;
    private string _title;

    public PushpinViewModel(IMessenger messenger) : base(messenger)
    {
    }

    /// <summary>
    /// Map id that pushpin represents
    /// </summary>
    public int MapTaskId { get; set; }

    /// <summary>
    /// Indicates if notification for task is enabled
    /// </summary>
    public bool NoticeEnabled { get; set; }

    /// <summary>
    /// Task title
    /// </summary>
    public string Title
    {
      get { return _title; }
      set
      {
        if (_title != value)
        {
          _title = value;
          RaisePropertyChanged(() => Title);
        }
      }
    }

    /// <summary>
    /// Geolocation of task
    /// </summary>
    [TypeConverter(typeof (GeoCoordinateConverter))]
    public GeoCoordinate Location
    {
      get { return _location; }
      set
      {
        if (_location != value)
        {
          _location = value;
          RaisePropertyChanged(() => Location);
        }
      }
    }

    /// <summary>
    /// Sends message when pushpin is tapped
    /// </summary>
    public ICommand TapCommand
    {
      get { return new RelayCommand(() => MessengerInstance.Send(new PushpinTappedMessage(MapTaskId))); }
    }

    /// <summary>
    /// Indicates is pushpin nearby to the current user location
    /// </summary>
    public bool IsNearby
    {
      get { return _isNearby; }
      set
      {
        if (_isNearby != value)
        {
          _isNearby = value;
          RaisePropertyChanged(() => IsNearby);
        }
      }
    }
  }
}