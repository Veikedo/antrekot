﻿namespace Antrekot.MainApp.ViewModel
{
  /// <summary>
  /// Represents model for culture
  /// </summary>
  public class LanguageViewModel
  {
    public LanguageViewModel(string name, string culture)
    {
      Name = name;
      Culture = culture;
    }

    /// <summary>
    /// Name of the language
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Culture of the language
    /// </summary>
    public string Culture { get; set; }
  }
}