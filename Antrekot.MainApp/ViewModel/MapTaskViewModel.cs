﻿using System;
using System.Windows.Input;
using Antrekot.MainApp.Helpers;
using Antrekot.MainApp.Messages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.ViewModel
{
  /// <summary>
  /// Represent view model for the task
  /// </summary>
  public class MapTaskViewModel : ViewModelBase
  {
    private string _description;
    private string _dmsNotation;
    private string _title;

    public MapTaskViewModel(IMessenger messenger) : base(messenger)
    {
    }

    /// <summary>
    /// Id of task in a repository
    /// </summary>
    public int TaskId { get; set; }

    /// <summary>
    /// Time of task creation
    /// </summary>
    public DateTime CreationTime { get; set; }

    /// <summary>
    /// Title of task
    /// </summary>
    public string Title
    {
      get { return _title; }
      set
      {
        if (_title != value)
        {
          _title = value;
          RaisePropertyChanged(() => Title);
        }
      }
    }

    /// <summary>
    /// Description of task
    /// </summary>
    public string Description
    {
      get { return _description; }
      set
      {
        if (_description != value)
        {
          _description = value;
          RaisePropertyChanged(() => Description);
        }
      }
    }

    /// <summary>
    ///   Gets the DMS (degree-minutes-sec) notation of
    /// </summary>
    public string DMSNotation
    {
      get { return _dmsNotation; }
      set
      {
        if (_dmsNotation != value)
        {
          _dmsNotation = value;
          RaisePropertyChanged(() => DMSNotation);
        }
      }
    }

    /// <summary>
    /// Sends message when model was tapped
    /// </summary>
    public ICommand TapCommand
    {
      get { return new RelayCommand(() => MessengerInstance.Send(new MapTaskViewModelTappedMessage(TaskId))); }
    }
  }
}