﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Antrekot.MainApp.Helpers;
using Antrekot.MainApp.Messages;
using Antrekot.MainApp.Resources;
using Antrekot.Repository.Models;
using Cimbalino.Phone.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.ViewModel.PageViewModels
{
  public class TaskDetailsPageViewModel : BaseViewModel
  {
    private readonly IMessageBoxService _messageBoxService;

    private bool _noticeEnabled;
    private string _pageTitle;
    private string _taskDMSNotation;

    #region fields

    private bool _canCommit;
    private MapTask _currentMapTask;
    private string _description;
    private string _title;

    #endregion

    #region Commands

    public ICommand CommitCommand
    {
      get { return new RelayCommand(Commit); }
    }

    public ICommand CancelCommand
    {
      get { return new RelayCommand(NavigationService.GoBack); }
    }

    public ICommand RemoveTaskCommand
    {
      get { return new RelayCommand(RemoveTask); }
    }

    #endregion

    public TaskDetailsPageViewModel(INavigationService navigationService, IMessageBoxService messageBoxService,
                                    IMessenger messenger)
      : base(navigationService, messenger)
    {
      _messageBoxService = messageBoxService;

      MessengerInstance.Register(this, (Action<ChangeMapTaskMessage>) ChangeMapTaskMessageReceived);
      PropertyChanged += OnPropertyChanged;
    }

    /// <summary>
    /// Title of current task
    /// </summary>
    public string Title
    {
      get { return _title; }
      set
      {
        if (_title != value)
        {
          _title = value;
          RaisePropertyChanged(() => Title);
        }
      }
    }

    /// <summary>
    /// Description of current task
    /// </summary>
    public string Description
    {
      get { return _description; }
      set
      {
        if (_description != value)
        {
          _description = value;
          RaisePropertyChanged(() => Description);
        }
      }
    }

    /// <summary>
    /// Indicates if notice enabled for current task
    /// </summary>
    public bool NoticeEnabled
    {
      get { return _noticeEnabled; }
      set
      {
        if (_noticeEnabled != value)
        {
          _noticeEnabled = value;
          RaisePropertyChanged(() => NoticeEnabled);
        }
      }
    }

    /// <summary>
    /// Indicates if user's input is correct
    /// </summary>
    public bool CanCommit
    {
      get { return _canCommit; }
      set
      {
        if (_canCommit != value)
        {
          _canCommit = value;
          RaisePropertyChanged(() => CanCommit);
        }
      }
    }

    /// <summary>
    /// Indicates if current task can be removed
    /// </summary>
    public bool CanRemove { get; set; }

    /// <summary>
    /// Title of the page. Can be "Add task" or "Edit task"
    /// </summary>
    public string PageTitle
    {
      get { return _pageTitle; }
      set
      {
        if (_pageTitle != value)
        {
          _pageTitle = value;
          RaisePropertyChanged(() => PageTitle);
        }
      }
    }

    /// <summary>
    /// DMS representation of map location
    /// </summary>
    public string TaskDMSNotation
    {
      get { return _taskDMSNotation; }
      set
      {
        if (_taskDMSNotation != value)
        {
          _taskDMSNotation = value;
          RaisePropertyChanged(() => TaskDMSNotation);
        }
      }
    }

    private void RemoveTask()
    {
      _messageBoxService.Show(AppResources.AreYouSure, AppResources.Delete,
                              new[] {"OK", AppResources.Cancel}, i =>
                              {
                                if (i == 0)
                                {
                                  var message = new RemoveMapTaskMessage(_currentMapTask.TaskId);
                                  MessengerInstance.Send(message);

                                  NavigateToMapPage();
                                }
                              });
    }

    private void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
    {
      CanCommit = !Title.IsNullOrEmpty();
    }

    private void ChangeMapTaskMessageReceived(ChangeMapTaskMessage message)
    {
      InitPage(message.MapTask, message.IsNewTask);
    }

    private void InitPage(MapTask mapTask, bool isNew)
    {
      _currentMapTask = mapTask;

      Title = _currentMapTask.Title;
      Description = _currentMapTask.Description;
      NoticeEnabled = isNew || mapTask.NoticeEnabled;
      CanRemove = !isNew;
      PageTitle = isNew ? AppResources.AddTask : AppResources.EditTask;
      TaskDMSNotation = CoordinateHelper.DMSNotation(mapTask.Latitude, mapTask.Longtitude);
    }

    private void Commit()
    {
      _currentMapTask.Title = Title;
      _currentMapTask.Description = Description;
      _currentMapTask.NoticeEnabled = NoticeEnabled;

      var message = new MapTaskChangedMessage(_currentMapTask);
      MessengerInstance.Send(message);

      NavigateToMapPage();
    }
  }
}