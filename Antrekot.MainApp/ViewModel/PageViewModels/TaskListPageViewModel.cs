﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Antrekot.MainApp.Helpers;
using Antrekot.MainApp.Messages;
using Antrekot.MainApp.Resources;
using Antrekot.Repository.Models;
using Antrekot.Repository.Repository;
using Cimbalino.Phone.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.ViewModel.PageViewModels
{
  public class TaskListPageViewModel : BaseViewModel
  {
    private readonly IMessageBoxService _messageBoxService;
    private readonly IRepository _repository;
    private ObservableCollection<MapTaskViewModel> _tasks;

    public TaskListPageViewModel(IRepository repository, INavigationService navigationService,
                                 IMessageBoxService messageBoxService, IMessenger messenger)
      : base(navigationService, messenger)
    {
      _repository = repository;
      _messageBoxService = messageBoxService;

      MessengerInstance.Register(this, (Action<MapTaskAddedMessage>) MapTaskAddedMessageReceived);
      MessengerInstance.Register(this, (Action<MapTaskRemovedMessage>) MapTaskRemovedMessageReceived);
      MessengerInstance.Register(this, (Action<MapTaskUpdatedMessage>) MapTaskUpdatedMessageReceived);
      MessengerInstance.Register(this, (Action<MapTaskViewModelTappedMessage>) MapTaskViewModelTappedMessageReceived);
    }

    /// <summary>
    ///   Calls page initialization method when page is loaded
    /// </summary>
    public ICommand PageLoadedCommand
    {
      get { return new RelayCommand(InitPage); }
    }

    /// <summary>
    ///   Sends message indicating all task needs to be removed
    /// </summary>
    public ICommand RemoveAllCommand
    {
      get { return new RelayCommand(RemoveAll); }
    }

    /// <summary>
    ///   List of tasks
    /// </summary>
    public ObservableCollection<MapTaskViewModel> Tasks
    {
      get { return _tasks; }
      set
      {
        if (_tasks != value)
        {
          _tasks = value;
          RaisePropertyChanged(() => Tasks);
        }
      }
    }

    private void OpenDetailsPageForEditTask(MapTask mapTask)
    {
      var editTaskMessage = new ChangeMapTaskMessage(mapTask);
      MessengerInstance.Send(editTaskMessage);

      NavigateToDetailsPage();
    }

    private void RemoveAll()
    {
      _messageBoxService.Show(AppResources.AreYouSure, AppResources.DeleteAllTasks,
                              new[] {"OK", AppResources.Cancel}, i =>
                              {
                                if (i == 0)
                                {
                                  var message = new RemoveAllMapTasksMessage();
                                  MessengerInstance.Send(message);

                                  Tasks.Clear(); // todo it's some ugly

                                  NavigateToMapPage();
                                }
                              });
    }

    private void MapTaskViewModelTappedMessageReceived(MapTaskViewModelTappedMessage message)
    {
      MapTask mapTask = _repository.MapTasks.First(x => x.TaskId == message.TaskId);
      OpenDetailsPageForEditTask(mapTask);
    }

    private void MapTaskAddedMessageReceived(MapTaskAddedMessage message)
    {
      var mapTask = message.MapTask;
      var model = MapToViewModel(mapTask);

      Tasks.Add(model);
    }

    private void MapTaskRemovedMessageReceived(MapTaskRemovedMessage message)
    {
      int index = Tasks.IndexOf(x => x.TaskId == message.MapTaskId);
      Tasks.RemoveAt(index);
    }

    private void MapTaskUpdatedMessageReceived(MapTaskUpdatedMessage message)
    {
      var mapTask = message.MapTask;
      MapTaskViewModel model = Tasks.First(x => x.TaskId == mapTask.TaskId);

      model.Title = mapTask.Title;
      model.Description = mapTask.Description;
    }

    private void InitPage()
    {
      bool notInitialized = Tasks == null;

      if (notInitialized)
      {
        var models = _repository.MapTasks.Select(MapToViewModel).OrderBy(x => x.CreationTime);
        Tasks = new ObservableCollection<MapTaskViewModel>(models);
      }
      else
      {
        Tasks = new ObservableCollection<MapTaskViewModel>(Tasks.OrderBy(x => x.CreationTime));
      }
    }

    private MapTaskViewModel MapToViewModel(MapTask mapTask)
    {
      return new MapTaskViewModel(MessengerInstance)
      {
        Title = mapTask.Title,
        Description = mapTask.Description,
        TaskId = mapTask.TaskId,
        CreationTime = mapTask.CreationTime,
        DMSNotation = CoordinateHelper.DMSNotation(mapTask.Latitude, mapTask.Longtitude)
      };
    }
  }
}