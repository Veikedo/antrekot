﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Antrekot.MainApp.Helpers;
using Antrekot.MainApp.Messages;
using Antrekot.MainApp.Services;
using Antrekot.Services.Settings;
using Cimbalino.Phone.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.ViewModel.PageViewModels
{
  public class SettingsPageViewModel : BaseViewModel
  {
    private readonly ISettingsService _settingsService;
    private LanguageViewModel _currentLanguage;
    private double _currentNoticeRadius;
    private ObservableCollection<LanguageViewModel> _languages;
    private double _noticeRadius;
    private LanguageViewModel _selectedLanguage;

    public SettingsPageViewModel(ISettingsService settingsService, INavigationService navigationService,
                                 IMessenger messenger)
      : base(navigationService, messenger)
    {
      _settingsService = settingsService;

      var eng = new LanguageViewModel("English", "en");
      var rus = new LanguageViewModel("Русский", "ru");

      Languages = new ObservableCollection<LanguageViewModel> {eng, rus};

      _selectedLanguage = _currentLanguage = _settingsService.Culture == "en" ? eng : rus;
      _noticeRadius = _currentNoticeRadius = MathHelper.MetresToKilometres(_settingsService.NoticeRadius);
    }

    /// <summary>
    /// If a task is located in this radius, notification would raised
    /// </summary>
    public double NoticeRadius
    {
      get { return _noticeRadius; }
      set
      {
        if (_noticeRadius != value)
        {
          _noticeRadius = value;
          RaisePropertyChanged(() => NoticeRadius);
        }
      }
    }

    /// <summary>
    /// Current locale
    /// </summary>
    public LanguageViewModel SelectedLanguage
    {
      get { return _selectedLanguage; }
      set
      {
        if (_selectedLanguage != value)
        {
          _selectedLanguage = value;
          RaisePropertyChanged(() => SelectedLanguage);
        }
      }
    }

    /// <summary>
    /// List of available languages
    /// </summary>
    public ObservableCollection<LanguageViewModel> Languages
    {
      get { return _languages; }
      set
      {
        if (_languages != value)
        {
          _languages = value;
          RaisePropertyChanged(() => Languages);
        }
      }
    }

    /// <summary>
    /// Accepts changes in settings
    /// </summary>
    public ICommand CommitCommand
    {
      get { return new RelayCommand(Commit); }
    }

    /// <summary>
    /// Rejects changes in settings
    /// </summary>
    public ICommand CancelCommand
    {
      get { return new RelayCommand(Cancel); }
    }

    private void Cancel()
    {
      SelectedLanguage = _currentLanguage;
      NoticeRadius = _currentNoticeRadius;

      NavigateToMapPage();
    }

    private void Commit()
    {
      SetCurrentCulture(SelectedLanguage.Culture);

      _currentLanguage = SelectedLanguage;
      _settingsService.Culture = SelectedLanguage.Culture;

      _currentNoticeRadius = NoticeRadius;
      _settingsService.NoticeRadius = MathHelper.KilometresToMetres(NoticeRadius);

      var message = new SettingsChangedMessage();
      MessengerInstance.Send(message);

      NavigateToMapPage();
    }
  }
}