﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Device.Location;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Threading;
using Antrekot.MainApp.Helpers;
using Antrekot.MainApp.Messages;
using Antrekot.MainApp.Services;
using Antrekot.Repository.Models;
using Antrekot.Repository.Repository;
using Antrekot.Services.Settings;
using Cimbalino.Phone.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Phone.Maps.Controls;

namespace Antrekot.MainApp.ViewModel.PageViewModels
{
  /// <summary>
  ///   DataContext for MapPage.xaml
  /// </summary>
  public class MapPageViewModel : BaseViewModel
  {
    #region fields

    private const double ZoomLevelStep = .5;
    private readonly ILocationService _locationService;
    private readonly IRepository _repository;
    private readonly ISettingsService _settingsService;
    private GeoCoordinate _currentLocation;
    private bool _currentLocationIsKnown;
    private bool _mapInitiated;
    private double _noticeRadius;
    private ObservableCollection<PushpinViewModel> _pushpins;
    private GeoCoordinate _visionPosition;
    private double _zoomLevel;

    #endregion

    public MapPageViewModel(IRepository repository, ILocationService locationService, ISettingsService settingsService,
                            INavigationService navigationService, IMessenger messenger)
      : base(navigationService, messenger)
    {
      _repository = repository;
      _locationService = locationService;
      _settingsService = settingsService;

      LoadSettings();
      _visionPosition = _currentLocation = GeoCoordinate.Unknown;

      MessengerInstance.Register(this, (Action<PushpinTappedMessage>) PushpinTappedMessageReceived);
      MessengerInstance.Register(this, (Action<MapTaskChangedMessage>) MapTaskChangedMessageReceived);
      MessengerInstance.Register(this, (Action<RemoveMapTaskMessage>) RemoveMapTaskMessageReceived);
      MessengerInstance.Register(this, (Action<RemoveAllMapTasksMessage>) RemoveAllMapTasksMessageReceived);
      MessengerInstance.Register(this, (Action<ApplicationClosingMessage>) ApplicationClosingMessageReceived);
      MessengerInstance.Register(this, (Action<SettingsChangedMessage>) SettingsChangedMessageReceived);

      NavigationService.Navigated += OnNavigated;
      _locationService.StatusChanged += LocationServiceOnStatusChanged;
    }

    // todo вынести этот bullshit в отдельную модель
    private void RemoveMapTask(int mapTaskId)
    {
      _repository.RemoveMapTask(mapTaskId);

      var message = new MapTaskRemovedMessage(mapTaskId);
      MessengerInstance.Send(message);

      int index = Pushpins.IndexOf(x => x.MapTaskId == mapTaskId);
      Pushpins.RemoveAt(index);
    }

    private void AddMapTask(GeoCoordinate coordinate)
    {
      var task = new MapTask
      {
        CreationTime = DateTime.Now,
        Latitude = coordinate.Latitude,
        Longtitude = coordinate.Longitude,
      };

      OpenDetailsPageForEditTask(task, isNew: true);
    }

    private void CommitMapTask(MapTask mapTask)
    {
      bool newTask = mapTask.TaskId == 0;
      if (newTask)
      {
        _repository.CreateMapTask(mapTask);

        var message = new MapTaskAddedMessage(mapTask);
        MessengerInstance.Send(message);

        var pushpin = BuildPushpinFromMapTask(mapTask);
        Pushpins.Add(pushpin);
      }
      else
      {
        _repository.UpdateMapTask(mapTask);

        var message = new MapTaskUpdatedMessage(mapTask);
        MessengerInstance.Send(message);

        PushpinViewModel pushpin = Pushpins.First(x => x.MapTaskId == mapTask.TaskId);
        pushpin.Title = mapTask.Title;
        pushpin.NoticeEnabled = mapTask.NoticeEnabled;
      }
    }

    private void RemoveAllMapTasks()
    {
      foreach (int taskId in _repository.MapTasks.Select(x => x.TaskId))
      {
        _repository.RemoveMapTask(taskId);
      }

      Pushpins.Clear();
    }

    #region helpers

    private void UpdatePushpins()
    {
      foreach (PushpinViewModel pushpin in Pushpins)
      {
        pushpin.IsNearby = IsNearbyMapTask(pushpin.Location); // todo make sound notice
      }
    }

    private bool IsNearbyMapTask(GeoCoordinate mapTaskLocation)
    {
      return mapTaskLocation.GetDistanceTo(_currentLocation) <= _noticeRadius;
    }

    private void LoadSettings()
    {
      _zoomLevel = _settingsService.ZoomLevel;
      _noticeRadius = _settingsService.NoticeRadius;

      SetCurrentCulture(_settingsService.Culture);
    }

    public override void Cleanup()
    {
      _locationService.PositionChanged -= PositionChanged;
      base.Cleanup();
    }

    private void OpenDetailsPageForEditTask(MapTask mapTask, bool isNew = false)
    {
      var editTaskMessage = new ChangeMapTaskMessage(mapTask, isNew);
      MessengerInstance.Send(editTaskMessage);

      NavigateToDetailsPage();
    }

    private PushpinViewModel BuildPushpinFromMapTask(MapTask mapTask)
    {
      var location = new GeoCoordinate(mapTask.Latitude, mapTask.Longtitude);
      return new PushpinViewModel(MessengerInstance)
      {
        Title = mapTask.Title,
        MapTaskId = mapTask.TaskId,
        Location = location,
        IsNearby = IsNearbyMapTask(location),
        NoticeEnabled = mapTask.NoticeEnabled
      };
    }

    #endregion

    #region props

    /// <summary>
    /// Source for the loader image
    /// </summary>
    public Uri MapLoaderImageSource
    {
      get { return new Uri("/Assets/Images/loader.gif", UriKind.Relative); }
    }


    /// <summary>
    /// indicates if map has already been loaded
    /// </summary>
    public bool MapInitiated
    {
      get { return _mapInitiated; }
      set
      {
        if (_mapInitiated != value)
        {
          _mapInitiated = value;
          RaisePropertyChanged(() => MapInitiated);
        }
      }
    }

    /// <summary>
    /// indicates if current user location is available
    /// </summary>
    public bool CurrentLocationIsKnown
    {
      get { return _currentLocationIsKnown; }
      set
      {
        if (_currentLocationIsKnown != value)
        {
          _currentLocationIsKnown = value;
          RaisePropertyChanged(() => CurrentLocationIsKnown);
        }
      }
    }

    /// <summary>
    /// Collection of created tasks pushpins
    /// </summary>
    public ObservableCollection<PushpinViewModel> Pushpins
    {
      get { return _pushpins; }
      set
      {
        if (_pushpins != value)
        {
          _pushpins = value;
          RaisePropertyChanged(() => Pushpins);
        }
      }
    }

    /// <summary>
    /// Returns current user location
    /// </summary>
    [TypeConverter(typeof (GeoCoordinateConverter))]
    public GeoCoordinate CurrentLocation
    {
      get { return _currentLocation; }
      set
      {
        if (_currentLocation != value)
        {
          _currentLocation = value;
          RaisePropertyChanged(() => CurrentLocation);
        }
      }
    }

    /// <summary>
    /// Current position of map viewport
    /// </summary>
    [TypeConverter(typeof (GeoCoordinateConverter))]
    public GeoCoordinate VisionPosition
    {
      get { return _visionPosition; }
      set
      {
        if (_visionPosition != value)
        {
          _visionPosition = value;
          RaisePropertyChanged(() => VisionPosition);
        }
      }
    }

    private static Dispatcher Dispatcher
    {
      get { return Deployment.Current.Dispatcher; }
    }

    /// <summary>
    /// Map's zoom level
    /// </summary>
    public double ZoomLevel
    {
      get { return _zoomLevel; }
      set
      {
        if (_zoomLevel != value)
        {
          _zoomLevel = value;
          RaisePropertyChanged(() => ZoomLevel);
        }
      }
    }

    #endregion

    #region Commands

    /// <summary>
    /// Opens task list page
    /// </summary>
    public ICommand OpenTaskListCommand
    {
      get { return new RelayCommand(NavigateToTaskListPage, () => Pushpins != null && Pushpins.Any()); }
    }

    /// <summary>
    /// opens settings page
    /// </summary>
    public ICommand OpenSettingsPageCommand
    {
      get { return new RelayCommand(NavigateToSettingsPage); }
    }

    /// <summary>
    /// Calls method for initiate map after it loading
    /// </summary>
    public ICommand MapLoadedCommand
    {
      get { return new RelayCommand(MapLoaded); }
    }


    /// <summary>
    /// Sets map viewport center to current user location
    /// </summary>
    public ICommand SetVisionToCurrentLocationCommand
    {
      get { return new RelayCommand(() => Dispatcher.BeginInvoke(() => VisionPosition = CurrentLocation)); }
    }

    /// <summary>
    /// Increases map zoom level
    /// </summary>
    public ICommand ZoomInCommand
    {
      get { return new RelayCommand(() => ZoomLevel += ZoomLevelStep); }
    }

    /// <summary>
    /// Decreases map zoom level
    /// </summary>
    public ICommand ZoomOutCommand
    {
      get { return new RelayCommand(() => ZoomLevel -= ZoomLevelStep); }
    }

    /// <summary>
    /// Adds new task and opens details page for edit it
    /// </summary>
    public ICommand AddMapTaskCommand
    {
      get { return new RelayCommand<GeoCoordinate>(AddMapTask); }
    }

    #endregion

    #region callbacks

    private void LocationServiceOnStatusChanged(object sender, LocationServiceStatusChangedEventArgs args)
    {
      Dispatcher.BeginInvoke(() =>
      {
        CurrentLocationIsKnown = args.Status == LocationServiceStatus.Ready;

        if (!CurrentLocationIsKnown)
        {
          foreach (var pushpin in Pushpins)
          {
            pushpin.IsNearby = false;
          }
        }
      });
    }

    private void OnNavigated(object sender, NavigationEventArgs args)
    {
      if (args.Uri.OriginalString == "/Views/MapPage.xaml")
      {
        while (NavigationService.CanGoBack)
        {
          NavigationService.RemoveBackEntry();
        }
      }
    }

    private void SettingsChangedMessageReceived(SettingsChangedMessage message)
    {
      _noticeRadius = _settingsService.NoticeRadius;
      UpdatePushpins();
    }

    private void ApplicationClosingMessageReceived(ApplicationClosingMessage message)
    {
      _settingsService.ZoomLevel = ZoomLevel;
    }

    private void RemoveAllMapTasksMessageReceived(RemoveAllMapTasksMessage message)
    {
      RemoveAllMapTasks();
    }

    private void RemoveMapTaskMessageReceived(RemoveMapTaskMessage message)
    {
      RemoveMapTask(message.MapTaskId);
    }

    private void MapTaskChangedMessageReceived(MapTaskChangedMessage message)
    {
      while (NavigationService.CanGoBack)
      {
        NavigationService.RemoveBackEntry();
      }

      CommitMapTask(message.MapTask);
    }

    private void PushpinTappedMessageReceived(PushpinTappedMessage message)
    {
      MapTask mapTask = _repository.MapTasks.First(x => x.TaskId == message.TaskId);
      OpenDetailsPageForEditTask(mapTask);
    }

    private void PositionChanged(object sender, LocationServicePositionChangedEventArgs args)
    {
      var location = new GeoCoordinate(args.Position.Latitude, args.Position.Longitude);
      Dispatcher.BeginInvoke(() =>
      {
        CurrentLocation = new GeoCoordinate(location.Latitude, location.Longitude);
        UpdatePushpins();
      });
    }

    private async void MapLoaded()
    {
      if (!MapInitiated)
      {
        var currentLocation = await _locationService.GetPositionAsync(LocationServiceAccuracy.Default);
        var coord = new GeoCoordinate(currentLocation.Latitude, currentLocation.Longitude);

        VisionPosition = coord;
        CurrentLocation = coord;

        var pushpins = _repository.MapTasks.Select(x => BuildPushpinFromMapTask(x));
        Pushpins = new ObservableCollection<PushpinViewModel>(pushpins);

        RaisePropertyChanged(() => OpenTaskListCommand); // hack to update button

        _locationService.PositionChanged += PositionChanged;
        _locationService.Start(LocationServiceAccuracy.High);

        MapInitiated = true;
      }
    }

    #endregion
  }
}