﻿using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating that task needs to be removed
  /// </summary>
  public class RemoveMapTaskMessage : MessageBase
  {
    public RemoveMapTaskMessage(int mapTaskId)
    {
      MapTaskId = mapTaskId;
    }

    public int MapTaskId { get; set; }
  }
}