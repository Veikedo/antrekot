﻿using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{

  /// <summary>
  /// Message indicating MapTaskViewModel has been tapped
  /// </summary>
  public class MapTaskViewModelTappedMessage : MessageBase
  {
    public MapTaskViewModelTappedMessage(int taskId)
    {
      TaskId = taskId;
    }

    public int TaskId { get; set; }
  }
}