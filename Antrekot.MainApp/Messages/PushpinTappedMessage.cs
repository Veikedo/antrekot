﻿using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating PushpinViewModel has been tapped
  /// </summary>
  public class PushpinTappedMessage : MessageBase
  {
    public PushpinTappedMessage(int taskId)
    {
      TaskId = taskId;
    }

    public int TaskId { get; set; }
  }
}