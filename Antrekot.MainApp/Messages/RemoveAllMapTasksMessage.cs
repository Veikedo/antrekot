﻿using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating that all tasks needs to be removed
  /// </summary>
  public class RemoveAllMapTasksMessage : MessageBase
  {
  }
}