﻿using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating the application is closing
  /// </summary>
  public class ApplicationClosingMessage : MessageBase
  {
  }
}