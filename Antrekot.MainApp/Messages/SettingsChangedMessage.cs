﻿using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating the settings has been changed
  /// </summary>
  public class SettingsChangedMessage : MessageBase
  {
  }
}