﻿using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating MapTask has been removed
  /// </summary>
  public class MapTaskRemovedMessage : MessageBase
  {
    public MapTaskRemovedMessage(int mapTaskId)
    {
      MapTaskId = mapTaskId;
    }

    public int MapTaskId { get; private set; }
  }
}