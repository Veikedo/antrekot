using Antrekot.Repository.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating the MapTask needs to be changed
  /// </summary>
  public class ChangeMapTaskMessage : MessageBase
  {
    public ChangeMapTaskMessage(MapTask mapTask, bool isNewTask = false)
    {
      MapTask = mapTask;
      IsNewTask = isNewTask;
    }

    public MapTask MapTask { get; private set; }
    public bool IsNewTask { get; private set; }
  }
}