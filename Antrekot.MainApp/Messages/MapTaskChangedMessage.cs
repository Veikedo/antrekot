﻿using Antrekot.Repository.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating the MapTask has been changed
  /// </summary>
  public class MapTaskChangedMessage : MessageBase
  {
    public MapTaskChangedMessage(MapTask mapTask)
    {
      MapTask = mapTask;
    }

    public MapTask MapTask { get; set; }
  }
}