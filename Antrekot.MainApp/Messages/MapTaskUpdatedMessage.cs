﻿using Antrekot.Repository.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{


  public class MapTaskUpdatedMessage : MessageBase
  {
    /// <summary>
    /// Message indicating MapTask has been updated in the database
    /// </summary>
    public MapTaskUpdatedMessage(MapTask mapTask)
    {
      MapTask = mapTask;
    }

    public MapTask MapTask { get; private set; }
  }
}