﻿using Antrekot.Repository.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Antrekot.MainApp.Messages
{
  /// <summary>
  /// Message indicating that new MapTask has been added
  /// </summary>
  public class MapTaskAddedMessage : MessageBase
  {
    public MapTaskAddedMessage(MapTask mapTask)
    {
      MapTask = mapTask;
    }

    public MapTask MapTask { get; private set; }
  }
}