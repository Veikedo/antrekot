﻿using System;
using System.Collections.Generic;

namespace Antrekot.MainApp.Helpers
{
  public static class EnumerableExtensions
  {
    /// <summary>
    /// Returns zero-based index of first matched element in the source.
    /// If element is not in the source returns -1
    /// </summary>
    public static int IndexOf<T>(this IEnumerable<T> source, Func<T, bool> match)
    {
      int idx = 0;
      foreach (T t in source)
      {
        if (match(t))
        {
          return idx;
        }

        idx = idx + 1; // todo ha-ha Макс не читает код
      }

      return -1;
    }
  }
}