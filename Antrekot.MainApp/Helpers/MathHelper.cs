﻿using System.Diagnostics.Contracts;

namespace Antrekot.MainApp.Helpers
{
  public class MathHelper
  {
    public static double Clamp(double value, double min, double max)
    {
      Contract.Requires(min <= max);
      return value < min ? min :
               value > max ? max : value;
    }

    public static double MetresToKilometres(double metres)
    {
      Contract.Requires(metres >= 0);
      return metres/1000;
    }

    public static double KilometresToMetres(double kilometres)
    {
      Contract.Requires(kilometres >= 0);
      return kilometres*1000;
    }
  }
}