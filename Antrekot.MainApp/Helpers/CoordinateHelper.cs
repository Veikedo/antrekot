﻿using System;

namespace Antrekot.MainApp.Helpers
{
  public static class CoordinateHelper
  {
    /// <summary>
    /// Returns DMS representation of coordinate
    /// </summary>
    public static string DMSNotation(double latitude, double longitude)
    {
      string latitudeDms = DMSNotationForSingleCoord(latitude, CoordTypes.Latitude);
      string longitudeDms = DMSNotationForSingleCoord(longitude, CoordTypes.Longitude);

      return string.Format("{0} {1}", latitudeDms, longitudeDms);
    }

    private static string DMSNotationForSingleCoord(double coord, CoordTypes coordType)
    {
      double posCoord = Math.Abs(coord);

      double degrees = Math.Truncate(posCoord);
      double t = (posCoord - degrees)*60;

      double minutes = Math.Truncate(t);
      double seconds = Math.Round((t - minutes)*60);

      char symbol;
      bool sign = coord < 0;

      if (coordType == CoordTypes.Latitude)
      {
        symbol = sign ? 'S' : 'N';
      }
      else
      {
        symbol = sign ? 'W' : 'E';
      }

      return string.Format("{0}°{1}′{2}{3}", degrees, minutes, seconds, symbol);
    }

    private enum CoordTypes
    {
      Latitude,
      Longitude
    }
  }
}