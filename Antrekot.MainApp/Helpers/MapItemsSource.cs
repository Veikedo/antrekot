﻿using System.Collections;
using System.Linq;
using System.Windows;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Maps.Toolkit;

namespace Antrekot.MainApp.Helpers
{
  public class MapItemsSource
  {
    public static readonly DependencyProperty ItemsSourceProperty;

    static MapItemsSource()
    {
      ItemsSourceProperty = DependencyProperty.RegisterAttached("ItemsSource", typeof (IEnumerable),
                                                                typeof (MapItemsSource),
                                                                new PropertyMetadata(OnPropertyChanged));
    }

    private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var uie = (UIElement) d;
      var control = MapExtensions.GetChildren((Map) uie).OfType<MapItemsControl>().First();
      control.ItemsSource = (IEnumerable) e.NewValue;
    }

    public static IEnumerable GetItemsSource(DependencyObject obj)
    {
      return (IEnumerable) obj.GetValue(ItemsSourceProperty);
    }

    public static void SetItemsSource(DependencyObject obj, IEnumerable value)
    {
      obj.SetValue(ItemsSourceProperty, value);
    }
  }
}